#!/bin/sh
RTLDIR=../../t65/vhdl
BENCHDIR=../../bench/vhdl
GHDLFLAGS="--std=08 --ieee=synopsys"

ghdl -a $GHDLFLAGS $RTLDIR/T65_Pack.vhd
ghdl -a $GHDLFLAGS $RTLDIR/T65_ALU.vhd
ghdl -a $GHDLFLAGS $RTLDIR/T65_MCode.vhd
ghdl -a $GHDLFLAGS $RTLDIR/T65.vhd
ghdl -a $GHDLFLAGS $BENCHDIR/T65_resetrun.vhdl
ghdl -r $GHDLFLAGS t65_resetrun --wave=t65_resetrun.ghw
