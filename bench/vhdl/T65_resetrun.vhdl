library ieee;
use ieee.std_logic_1164.all;

entity T65_resetrun is
end T65_resetrun;

architecture rtl of T65_resetrun is
  signal Clk:           std_logic;
  signal Reset_n:       std_logic;
  
  constant CLK_PERIOD:  time := 10 ns;

  procedure ClkCycle(
    signal CLK: out std_logic;
    CLK_PERIOD: time
  ) is
  begin
    CLK <= '0';
    wait for CLK_PERIOD/4;
    CLK <= '1';
    wait for CLK_PERIOD/2;
    CLK <= '0';
    wait for CLK_PERIOD/4;
  end ClkCycle;

  procedure ClkCycles(
    N:  integer;
    signal CLK: out std_logic;
    CLK_PERIOD: time
  ) is
  begin
    for i in 1 to N loop
      ClkCycle(CLK, CLK_PERIOD);
    end loop;
  end ClkCycles;
begin
  CPU: entity work.T65
    port map (
      Mode => "00",
      Res_n => Reset_n,
      Enable => '1',
      Clk => Clk,
      Rdy => '1',
      Abort_n => '1',
      IRQ_n => '1',
      NMI_n => '1',
      SO_n => '1',
      R_W_n => open,
      Sync => open,
      EF => open,
      MF => open,
      XF => open,
      ML_n => open,
      VP_n => open,
      VDA => open,
      VPA => open,
      A => open,
      DI => x"EA", -- NOP
      DO => open,
      DEBUG => open
    );

  SIM: process
  begin
    -- Reset
    Clk <= '0';
    Reset_n <= '0';
    wait for 3*CLK_PERIOD;

    Reset_n <= '1';
    
    ClkCycles(12, Clk, CLK_PERIOD);

    -- End simulation
    wait;
  end process;
end rtl;

      
