#!/bin/sh
vhdldir=`realpath ../../t65/vhdl`
opts="--std=08 --ieee=synopsys -Wno-pragma"
rm -f *.cf
ghdl -a $opts $vhdldir/T65_Pack.vhd
ghdl -a $opts $vhdldir/T65_ALU.vhd
ghdl -a $opts $vhdldir/T65_MCode.vhd
ghdl -a $opts $vhdldir/T65.vhd
ghdl -r $opts T65 --no-run
yosys -m ghdl <<EOF
ghdl $opts t65
EOF
