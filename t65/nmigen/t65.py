import os
from enum import Enum

from nmigen import *
from nmigen.lib.io import *

from nmigen_soc.wishbone.bus import Interface

__all__ = [
    "T65Mode",
    "T65", "T65_WB", "MOS6502", "MOS6502_WB", "WDC65C816", "WDC65C816_WB"
]

class T65Mode(Enum):
    """The operation modes for the T65 block"""
    MOS6502 = 0
    WDC65C02 = 1
    WDC65C816 = 2


class T65(Elaboratable):
    """nMigen wrapper around T65 VHDL code.

    Args:
        domain (str): default is "sync"
            The clock domain in which the CPU will reside. This will replace functionality of
            original PHI0/PHI1/PHI2 pins.
        on_negedge (bool, optional): default is False
            If ``True`` inputs to the block will be latched on the falling edge
            of the clock and output will also change on the edge. This can allow the use
            of synchronous SRAMs as memory without the need for wait states. This is possible
            if one latches the address in the synchronous SRAM on the rising edge of the clock
            and the output of the SRAM available on the next falling edge of the clock.

    As is common in nMigen all logic is active high and active low signal on the CPU have been
    converted where needed.

    Attributes valid for all modes:
        mode (Signal(2)): mode of operation; default T65Mode.MOS6502. Assign value of
            :class:T65Mode to select in which CPU mode to run. Changing this value when not in
            reset is undefined behavior.
        ready (Signal(1)): input corresponding with RDY pin of CPU;
        irq (Signal(1)): input corresponding with IRQn/IRQB pin of CPU.
        nmi (Signal(1)): input corresponding with NMIn/NMIB pin of CPU.
        addr (Signal(24)): output in MOS6502/WDC65C02 mode correpsonds with A0-A15 pins of CPU
            and with A0-A23 in WDC65C816 mode.
        data (Record((("i", 8), ("o", 8), ("oe", 1))): the data connection. This corresponds with
            the D0-D7 pins of original CPU.
            The input value from a read has to assigned to the ``i`` field.
            The output to be written is given in the ``o`` field. In WDC65C816 this attribute is not
            used for address values.
            The ``oe`` field takes the functionality of the original 'R/W'/RWB pin. If high it
            indicates a write cycle otherwise a read cycle.

    Attributes valid for MOS6502/WDC65C02 modes:
        set_overflow (Signal(1)): corresponds with SO pin of original CPU.
        sync (Signal(1)): corresponds with SYNC signal of original CPU.

    Attributes valid for WDC65C02/WDC65C816 modes:
        vector_pull (Signal(1)): corresponds with VP pin of original CPU
        mem_lock (Signal(1)): corresponds with ML pin of original CPU

    Atrributes valid for WDC65C816 mode:
        valid_addr (Signal(2)): output; bit 0 corresponds with VDA original CPU pin and bit 1
             with VPA pin.

    Extra attributes:
        ce (Signal(1)): if not asserted clock to CPU is gated; at reset signal will be asserted.
        en (Signal(1)): if not asserted CPU will be kept in reset state; at reset signal will be
            asserted.

    The BE pin present on WDC65C02/WDC65C816 does not have a nMigen equivalent. If tristate logic
    is needed on a higher level it has to be implemented there.
    The original CPU pins Abort_b, M/X and E are (currently) not handled by nMigen wrapper.

    """
    @staticmethod
    def _add_files(platform, prefix):
        d = os.path.realpath("{dir}{sep}{par}{sep}vhdl".format(
            dir=os.path.dirname(__file__), sep=os.path.sep, par=os.path.pardir
        )) + os.path.sep
        for fname in [
            "T65_Pack.vhd",
            "T65_MCode.vhd",
            "T65_ALU.vhd",
            "T65.vhd",
        ]:
            f = open(d + fname, "r")
            platform.add_file(prefix + fname, f)
            f.close()


    def __init__(self, *, domain="sync", on_negedge=False):
        self.mode = Signal(2, reset=T65Mode.MOS6502.value)
        self.ready = Signal(reset=1)
        self.irq = Signal()
        self.nmi = Signal()
        self.addr = Signal(24)
        self.data = Record((("i", 8), ("o", 8), ("oe", 1)))
        self.set_overflow = Signal()
        self.sync = Signal()
        self.vector_pull = Signal()
        self.mem_lock = Signal()
        self.valid_addr = Signal(2)
        self.ce = Signal(reset=1) # If ce is 0, the chip won't be clocked
        self.en = Signal(reset=1) # If not enables chip will be kept in reset state

        self._domain = domain
        self._on_negedge = on_negedge

    def elaborate(self, platform):
        self.__class__._add_files(platform, "t65/")

        m = Module()

        clk = Signal()
        reset_n = Signal()
        irq_n = Signal()
        nmi_n = Signal()
        so_n = Signal()
        r_w_n = Signal()
        ml_n = Signal()
        vp_n = Signal()

        m.d.comb += [
            clk.eq(ClockSignal(self._domain) if not self._on_negedge else ~ClockSignal(self._domain)),
            reset_n.eq(~(ResetSignal(self._domain) | ~self.en)),
            irq_n.eq(~self.irq),
            nmi_n.eq(~self.nmi),
            so_n.eq(~self.set_overflow),
            self.data.oe.eq(~r_w_n),
            self.mem_lock.eq(~ml_n),
            self.vector_pull.eq(~vp_n),
        ]
        m.submodules.t65_ = Instance("t65",
            i_mode = self.mode,
            i_res_n = reset_n,
            i_enable = self.ce,
            i_clk = clk,
            i_rdy = self.ready,
            i_irq_n = irq_n,
            i_nmi_n = nmi_n,
            i_so_n = so_n,
            o_r_w_n = r_w_n,
            o_sync = self.sync,
            o_ml_n = ml_n,
            o_vp_n = vp_n,
            o_vda = self.valid_addr[0],
            o_vpa = self.valid_addr[1],
            o_a = self.addr,
            i_di = self.data.i,
            o_do = self.data.o,
        )

        return m

class T65_WB(Elaboratable):
    """nMigen wrapper around T65 VHDL code with Wishbone interface.

    This is based on :class:`T65` but some of the attributes are now handled by a Wishbone
    interface. :attr:`T65.addr`, :attr:`T65.data`, :attr:`T65.ready` are now handled by a
    :attr:`bus` attribute that is a Wishbone interface with "stall" and "lock" features.

    For the parameters :arg:`domain` and :arg:`on_negedge` and the attributes :attr:`mode`,
    :attr:`sync', :attr:`set_overflow`, :attr:`vector_pull`, :attr:`valid_addr`, :attr:`irq`,
    :attr:`nmi`, :attr:`ce` and :attr:`en` see documentation of :class:`T65`.
    """
    def __init__(self, *, domain="sync", on_negedge=True):
        self._t65 = t65 = T65(domain=domain, on_negedge=on_negedge)
        self.bus = Interface(data_width=8, addr_width=24, features=("stall", "lock"))

        self.mode = t65.mode
        self.sync = t65.sync
        self.set_overflow = t65.set_overflow
        self.vector_pull = t65.vector_pull
        self.valid_addr = t65.valid_addr
        self.irq = t65.irq
        self.nmi = t65.nmi
        self.ce = t65.ce
        self.en = t65.en

    def elaborate(self, platform):
        wb = self.bus
        t65 = self._t65

        m = Module()
        m.submodules.t65_ = t65

        m.d.comb += [
            wb.adr.eq(t65.addr),
            wb.dat_w.eq(t65.data.o),
            t65.data.i.eq(wb.dat_r),
            wb.we.eq(t65.data.oe),
            wb.lock.eq(t65.mem_lock),
        ]

        # Do read or write cycle each clock cycle
        # Let CPU wait when stall is high or no ack is present after first cycle.
        first = Signal(reset=1)
        m.d.sync += first.eq(Const(0))
        m.d.comb += [
            t65.ready.eq(~wb.stall & (first | wb.ack)),
            wb.cyc.eq(self.en),
            wb.stb.eq(self.en),
        ]

        return m


class MOS6502(Elaboratable):
    """This is :class:`T65` but with a mode fixed to a 6502 8-bit mode

    Args:
        cmos (bool): wether to use the CMOS 6502 or not.
        domain (str): see :class:`T65`, default is "sync"
        on_negedge (bool): see :class:`T65`, default is False

    Only attributes that are valid of MOS6502 or WDC65C02 are present for an object
    of this class.
    """
    def __init__(self, *, cmos=False, domain="sync", on_negedge=False):
        self._t65 = t65 = T65(domain=domain, on_negedge=on_negedge)

        self.ready = t65.ready
        self.irq = t65.irq
        self.nmi = t65.nmi
        self.addr = Signal(16)
        self.data = t65.data
        self.set_overflow = t65.set_overflow
        self.sync = t65.sync
        if cmos:
            self.vector_pull = t65.vector_pull
            self.mem_lock = t65.mem_lock
        self.ce = t65.ce
        self.en = t65.en

        ##

        self._cmos = cmos

    def elaborate(self, platform):
        t65 = self._t65

        m = Module()
        m.submodules.t65_ = t65

        m.d.comb += [
            t65.mode.eq(T65Mode.MOS6502 if not self._cmos else T65Mode.WDC65C02),
            self.addr.eq(t65.addr[:16]),
        ]

        return m

class MOS6502_WB(Elaboratable):
    """This is :class:`T65_WB` but with a mode fixed to a 6502 8-bit mode

    Args:
        cmos (bool): wether to use the CMOS 6502 or not.
        domain (str): see :class:`T65`, default is "sync"
        on_negedge (bool): see :class:`T65`, default is False

    Only attributes that are valid of MOS6502 or WDC65C02 are present for an object
    of this class.
    """
    def __init__(self, *, cmos=False, domain="sync", on_negedge=True):
        self._t65 = t65 = T65_WB(domain=domain, on_negedge=on_negedge)
        # Have own bus with 16 bit address width
        self.bus = Interface(data_width=8, addr_width=16, features=("stall", "lock"))

        self.sync = t65.sync
        self.set_overflow = t65.set_overflow
        if cmos:
            self.vector_pull = t65.vector_pull
        self.irq = t65.irq
        self.nmi = t65.nmi
        self.ce = t65.ce
        self.en = t65.en

        ##

        self._cmos = cmos

    def elaborate(self, platform):
        wb = self.bus
        t65 = self._t65

        m = Module()
        m.submodules.t65_ = t65

        m.d.comb += [
            wb.adr.eq(t65.bus.adr[:16]),
            t65.bus.dat_r.eq(wb.dat_r),
            wb.dat_w.eq(t65.bus.dat_w),
            wb.we.eq(t65.bus.we),
            t65.bus.ack.eq(wb.ack),
            wb.cyc.eq(t65.bus.cyc),
            wb.stb.eq(t65.bus.stb),
            t65.bus.stall.eq(wb.stall),
            wb.lock.eq(t65.bus.lock),
            t65.mode.eq(T65Mode.MOS6502 if not self._cmos else T65Mode.WDC65C02),
        ]

        return m


class WDC65C816(Elaboratable):
    """This is :class:`T65` but with a mode fixed to WDC65C816

    For arguments :arg:`domain` and :arg:`on_negedge` see documentation of :class:`T65`.
    Only attributes that are valid for WDC65C816 are present for an objectof this class.
    """
    def __init__(self, *, domain="sync", on_negedge=False):
        self._t65 = t65 = T65_WB(domain=domain, on_negedge=on_negedge)

        self.ready = t65.ready
        self.irq = t65.irq
        self.nmi = t65.nmi
        self.addr = t65.addr
        self.data = t65.data
        self.vector_pull = t65.vector_pull
        self.mem_lock = t65.mem_lock
        self.valid_addr = t65.valid_addr
        self.ce = t65.ce
        self.en = t65.en

    def elaborate(self, platform):
        t65 = self._t65

        m = Module()
        m.submodules.t65_ = t65

        m.d.comb += t65.mode.eq(T65Mode.WDC65C816)

        return m

class WDC65C816_WB(Elaboratable):
    """This is :class:`T65_WB` but with a mode fixed to WDC65C816

    For arguments :arg:`domain` and :arg:`on_negedge` see documentation of :class:`T65_WB`.
    Only attributes that are valid for WDC65C816 are present for an objectof this class.
    """
    def __init__(self, *, domain="sync", on_negedge=True):
        self._t65 = t65 = T65_WB(domain=domain, on_negedge=on_negedge)
        self.bus = t65.bus

        self.sync = t65.sync
        self.set_overflow = t65.set_overflow
        self.vector_pull = t65.vector_pull
        self.valid_addr = t65.valid_addr
        self.irq = t65.irq
        self.nmi = t65.nmi
        self.ce = t65.ce
        self.en = t65.en

    def elaborate(self, platform):
        wb = self.bus
        t65 = self._t65

        m = Module()
        m.submodules.t65_ = t65

        m.d.comb += t65.mode.eq(T65Mode.WDC65C816)

        return m
